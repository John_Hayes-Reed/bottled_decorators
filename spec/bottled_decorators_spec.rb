require 'spec_helper'
require 'json'

describe "Bottled Decorators Gem" do
  it 'has a version number' do
    expect(BottledDecorators::VERSION).not_to be nil
  end
end

describe BottledDecorator do
  it 'can successfully stack the return value when overriding methods in lower parts of the wrapper stack and using super to build upon the lower stacks returned value' do
    # Arrange
    class FirstStackTestDecorator
      include BottledDecorator
      def number
        super + 1
      end
    end
    
    class SecondStackTestDecorator
      include BottledDecorator
      def number
        super + 2
      end
    end

    class StackedTest
      def number
        1
      end
    end

    decorated_object = SecondStackTestDecorator.call(FirstStackTestDecorator.call(StackedTest.new))

    # Act
    result = decorated_object.number

    # Assert
    expect(result).to eq 4
  end

  it 'can access methods available in the decorator class, and the class of the original component' do
    # Arrange
    class FullNameTestDecorator
      include BottledDecorator
      def full_name
        first_name + " " + last_name
      end
    end
    
    class PersonTest
      attr_accessor :first_name, :last_name
      def initialize(first_name:, last_name:)
        @first_name = first_name
        @last_name = last_name
      end
    end

    # Act
    decorated_object = FullNameTestDecorator.call(PersonTest.new(first_name: "John", last_name: "Smith"))
    first_name = decorated_object.first_name
    last_name = decorated_object.last_name
    full_name = decorated_object.full_name

    # Assert
    aggregate_failures do
      expect(first_name).to eq "John"
      expect(last_name).to eq "Smith"
      expect(full_name).to eq "John Smith"
    end
  end

  it 'can access methods from all stacked decorators in the chain and also access methods from the original component' do
    # Arrange
    class OuterTestDecorator
      include BottledDecorator
      def foo; :foo; end
    end

    class MiddleTestDecorator
      include BottledDecorator
      def bar; :bar; end
    end

    class InnerTestDecorator
      include BottledDecorator
      def baz; :baz; end
    end

    class RootTestClass
      def baq; :baq; end
    end

    # Act
    decorated_object = OuterTestDecorator.call(MiddleTestDecorator.call(InnerTestDecorator.call(RootTestClass.new)))
    outer_result = decorated_object.foo
    middle_result = decorated_object.bar
    inner_result = decorated_object.baz
    root_result = decorated_object.baq

    # Assert
    aggregate_failures do
      expect(outer_result).to eq :foo
      expect(middle_result).to eq :bar
      expect(inner_result).to eq :baz
      expect(root_result).to eq :baq
    end
  end

  it "can accept additional optional variables when using #call, and access those variables through reader methods on the instance" do
    # Arrange
    class OptionalVariableTestDecorator
      include BottledDecorator
    end

    class OptionalVariableTestDecoratedClass
    end

    # Act
    decorated_object = OptionalVariableTestDecorator.call(OptionalVariableTestDecoratedClass.new, test_var: :test)
    test_var = decorated_object.test_var

    # Assert
    expect(test_var).to eq :test
  end

  it "can accept and access additional option variables from all layers of the stacked decorator chain" do
    # Arrange
    class OptionalVariableStackOneTestDecorator
      include BottledDecorator
    end

    class OptionalVariableStackTwoTestDecorator
      include BottledDecorator
    end

    class OptionalVariableTestStackedDecoratedClass
    end

    # Act
    decorated_object = OptionalVariableStackTwoTestDecorator.call(OptionalVariableStackOneTestDecorator.call(OptionalVariableTestDecoratedClass.new, test_var_one: :test_one), test_var_two: :test_two)
    test_var_one = decorated_object.test_var_one
    test_var_two = decorated_object.test_var_two

    # Assert
    aggregate_failures do
      expect(test_var_one).to eq :test_one
      expect(test_var_two).to eq :test_two
    end
  end

  it "can be serialised to JSON and a Hash" do
    # Arrange
    class JsonSerialisationTestDecorator
      include BottledDecorator
      def full_name
        first_name + " " + last_name
      end
    end

    class JsonSerialisationTestPerson
      def first_name; "John"; end
      def last_name; "Smith"; end
      def as_json(args); JSON.parse({ first_name: 'John', last_name: 'Smith' }.to_json); end
    end

    decorated_object = JsonSerialisationTestDecorator.call(JsonSerialisationTestPerson.new, test: :test_val)

    # Act
    to_json_result = decorated_object.to_json
    to_h_result = decorated_object.to_h

    # Assert
    aggregate_failures do
      expect(to_h_result).to eq({ 'first_name' => 'John', 'last_name' => 'Smith', 'full_name' => 'John Smith' })
      expect(to_json_result).to eq("{\"first_name\":\"John\",\"last_name\":\"Smith\",\"full_name\":\"John Smith\"}")
    end
  end

  it "responds to methods provided by both the decorators and the original component" do
    # Arrange
    class RespondToTestDecorator
      include BottledDecorator
      def foo; :foo; end
    end

    class RespondToTestClass
      def bar; :bar; end
    end

    decorated_object = RespondToTestDecorator.call(RespondToTestClass.new)

    # Act & Assert
    aggregate_failures do
      expect(decorated_object.respond_to?(:foo)).to eq(true)
      expect(decorated_object.respond_to?(:bar)).to eq(true)
      expect(decorated_object.respond_to?(:baz)).to eq(false)
    end
  end

  it "should decorate all items of a collection when passed an Enumarable collection" do
    # Arrange
    class EnumerableCollectionTestDecorator
      include BottledDecorator
      def foo; :foo; end
    end

    class EnumerableCollectionTestClass
    end

    decorated_objects = EnumerableCollectionTestDecorator.call([EnumerableCollectionTestClass.new, EnumerableCollectionTestClass.new])

    # Act & Assert
    aggregate_failures do
     expect(decorated_objects).to be_a Array
     decorated_objects.each do |obj|
       expect(obj.respond_to?(:foo)).to eq true
       expect(obj.foo).to eq :foo
     end
    end
  end
end
